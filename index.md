---
layout: default
title: Inici
---
<!-- Header with full-height image -->
<header class="bgimg-1 w3-display-container w3-grayscale-min" id="home">
  <div class="w3-display-left w3-text-white" style="padding:48px">
    <span class="w3-jumbo">SEGELL REPUBLICÀ</span><br>
    <span class="w3-large">Cerca les empreses amb segell. Anuncia que tens segell.</span>
    <p><a href="#about" class="w3-button w3-white w3-padding-large w3-large w3-margin-top w3-opacity w3-hover-opacity-off">Què és el segell?</a></p>
  </div> 
  <div class="w3-display-bottomleft w3-text-grey w3-large" style="padding:24px 48px">
    <i class="fa fa-facebook-official w3-hover-opacity"></i>
    <i class="fa fa-instagram w3-hover-opacity"></i>
    <i class="fa fa-snapchat w3-hover-opacity"></i>
    <i class="fa fa-pinterest-p w3-hover-opacity"></i>
    <i class="fa fa-twitter w3-hover-opacity"></i>
    <i class="fa fa-linkedin w3-hover-opacity"></i>
  </div>
</header>

<!-- About Section -->
<div class="w3-container" style="padding:128px 16px" id="about">
  <h3 class="w3-center">QUÈ ÉS EL SEGELL?</h3>
  <p class="w3-center w3-large">Com funciona?</p>
  <div class="w3-row-padding w3-center" style="margin-top:64px">
    <div class="w3-quarter">
      <i class="fa fa-desktop w3-margin-bottom w3-jumbo w3-center"></i>
      <p class="w3-large">Simple</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
    </div>
    <div class="w3-quarter">
      <i class="fa fa-heart w3-margin-bottom w3-jumbo"></i>
      <p class="w3-large">Amigable</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
    </div>
    <div class="w3-quarter">
      <i class="fa fa-diamond w3-margin-bottom w3-jumbo"></i>
      <p class="w3-large">Qualitat</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
    </div>
    <div class="w3-quarter">
      <i class="fa fa-cog w3-margin-bottom w3-jumbo"></i>
      <p class="w3-large">Clar i català</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
    </div>
  </div>
</div>

<!-- cercador -->
<div class="w3-container w3-light-grey" style="padding:128px 16px" id="cercador">
  <div class="w3-row-padding">
    <div class="w3-col m6">
      <h3>Cercador d'empreses</h3>
      <p>Cerca mitjançant diferents filtres</p>
      <p><a href="/cercador.html" class="w3-button w3-black"><i class="fa fa-th"> </i> Cercador</a></p>
    </div>
    <div class="w3-col m6">
      <img class="w3-image w3-round-large" src="img/demo.jpg" alt="cercador" width="700" height="394">
    </div>
  </div>
</div>



<!-- ADHESIO Section -->
<div class="w3-container w3-center w3-dark-grey" style="padding:128px 16px" id="adhesio">
  <h3>ADHESIÓ</h3>
  <p class="w3-large">Formulari d'adhesió per a les empreses.</p>
  <div class="w3-row-padding" style="margin-top:64px">
    <div class="w3-center w3-section">
    <form action="https://staticman3.herokuapp.com/v3/entry/gitlab/segellrepublica/registre/master/comments" method="POST">
      <ul class="w3-ul w3-white w3-hover-shadow">
        <li class="w3-black w3-xlarge w3-padding-32">Formulari d'adhesió</li>
        <li class="w3-padding-16"><input class="w3-input w3-border" type="text" placeholder="Nom" required name="fields[Name]"></li>
        <li class="w3-padding-16"><input class="w3-input w3-border" type="text" placeholder="Correu electrònic" required name="fields[Email]"></li>
        <li class="w3-padding-16"><input class="w3-input w3-border" type="text" placeholder="Text descriptiu" required name="fields[Text]"></li>
        <li class="w3-light-grey w3-padding-24">
          <button class="w3-button w3-black w3-padding-large" type="submit">Registra</button>
        </li>
      </ul>
    </form>
    </div>
  </div>
</div>

<!-- Contact Section -->
<div class="w3-container w3-light-grey" style="padding:128px 16px" id="contact">
  <h3 class="w3-center">CONTACTE</h3>
  <p class="w3-center w3-large">Contacteu amb nosaltres. Envieu-nos un missatge:</p>
  <div style="margin-top:48px">
    <p><i class="fa fa-map-marker fa-fw w3-xxlarge w3-margin-right"></i> Països Catalans</p>
    <p><i class="fa fa-phone fa-fw w3-xxlarge w3-margin-right"></i> Telèfon: 123456789</p>
    <p><i class="fa fa-envelope fa-fw w3-xxlarge w3-margin-right"> </i> Correu: mail@example.com</p>
    <br>
    <form action="https://getsimpleform.com/messages?form_api_token=0c74372a7fa5d1faad90157ef0d26c77" method="post">
  <!-- the redirect 
  <input type='hidden' name='redirect_to' value='https://segellrepublica.gitlab.io' />-->
      <p><input class="w3-input w3-border" type="text" placeholder="Nom" required name="Name"></p>
      <p><input class="w3-input w3-border" type="text" placeholder="Correu electrònic" required name="Email"></p>
      <p><input class="w3-input w3-border" type="text" placeholder="Assumpte" required name="Subject"></p>
      <p><input class="w3-input w3-border" type="text" placeholder="Missatge" required name="Message"></p>
      <p>
        <button class="w3-button w3-black" type="submit">
          <i class="fa fa-paper-plane"></i> ENVIA
        </button>
      </p>
    </form>
  </div>
</div>

<!-- Statistics -->
<div class="w3-container w3-row w3-center w3-dark-grey w3-padding-64">
  <div class="w3-quarter">
    <span class="w3-xxlarge">1714</span>
    <br>Empreses
  </div>
  <div class="w3-quarter">
    <span class="w3-xxlarge">155</span>
    <br>Xerrades
  </div>
  <div class="w3-quarter">
    <span class="w3-xxlarge">10</span>
    <br>Fires
  </div>
  <div class="w3-quarter">
    <span class="w3-xxlarge">1</span>
    <br>Independència
  </div>
</div>
