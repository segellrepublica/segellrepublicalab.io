---
layout: default
title: Cercador
head: faceted.html
---

{% raw %}
<div id="el">
 

  <div class="w3-container">

    <h1>Empreses ({{ searchResult.pagination.total }})</h1>

    <p class="text-muted">Search performed in {{ searchResult.timings.search }} ms, facets in {{ searchResult.timings.facets }} ms
    <br/>
        <a class="navbar-brand" href="#" v-on:click="reset()">Reset</a>
</p>


<div class="w3-display-container">
        <form class="w3-left">
            <input type="text" v-model="query" class="form-control" placeholder="Cerca">
        </form>
<!-- div for pagination block -->

      <paginate
                :page-count="Math.ceil(searchResult.pagination.total / searchResult.pagination.per_page)"
                :click-handler="goToPage"
                :prev-text="'Prev'"
                :next-text="'Next'"
                :container-class="'pagination w3-border w3-round w3-right'">
      </paginate>

</div>

    <div class="w3-row">
      <div class="w3-third">
        <div v-for="facet in searchResult.data.aggregations">
          <h5 style="margin-bottom: 5px;"><strong style="color: #337ab7;">{{ facet.title }}</strong></h5>

          <ul class="browse-list list-unstyled long-list" style="margin-bottom: 0;">
            <li v-for="bucket in facet.buckets">
            <div class="checkbox block" style="margin-top: 0; margin-bottom: 0;">
              <label>
                <!--<input class="checkbox" type="checkbox" v-on:click="updateFilters(facet.name, bucket.key)" v-model="filters[bucket.key]" value="{{ bucket.key }}" v-bind:value="isChecked2()">-->
                <!--<input class="checkbox" type="checkbox" v-on:click="updateFilters(facet.name, bucket.key)" v-model="filters[bucket.key]" v-bind:value="bucket.key">-->
                <input class="checkbox" type="checkbox" v-model="filters[facet.name]" v-bind:value="bucket.key">
                {{ bucket.key }} ({{ bucket.doc_count }}) 
              </label>
            </div>
            </li>
          </ul>
        </div>
      </div>

      <div class="w3-twothird">
        <div class="breadcrumbs"></div>
        <div class="clearfix"></div>
        <!--<h3>List of items ({{ searchResult.pagination.total }})</h3>-->
        <table class="table table-striped">
          <tbody>
          <tr v-for="item of searchResult.data.items">
            <td><img style="width: 100px;" v-bind:src="item.imatge"></td>
            <td></td>
            <td>
              <a v-bind:href="'/collectius/'+item._id"><b>{{ item.nom }}</b></a>
              <br />
              {{ item.web }}
            </td>
            <td></td>
            <td>
              <span style="font-size: 12px; display: block; float: left; background-color: #dbebf2; border-radius: 5px; padding: 1px 3px 1px 3px; margin: 2px;" v-for="tipus in item.tipus">{{ tipus }}</span>
            </td>
          </tr>
          </tbody>
        </table>
        <div class="clearfix"></div>
      </div>

      <div class="clearfix" style="margin-bottom: 100px;"></div>
    </div>
  </div>
</div>
{% endraw %}
